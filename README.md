# Kneser

## src/7_check.jl

Tries to check that all irreducible representations other than those of the form `(n-r+a,r-a)` have zero-sum and therefore can be omitted.

The representation theory uses this [package](https://github.com/dlfivefifty/RepresentationTheory.jl).

## src/7_check_GAP.jl
The same as `src/7_check.jl`, but using `GAP` instead of `RepresentationTheory.jl` package.

## src/c_i.jl
Computes c<sub>i</sub>-ies. Meaning that given Kneser graph `KG(n,r,k)` it computes one group element c<sub>i</sub> for every neighbor <em>i</em> of vertex `(1,2,..,r)` for which c<sub>i</sub>((1,2,..,r)) = <em>i</em>.

## src/kneser-general.jl
Rewritten semidefinite program from [this website](http://citmalumnes.upc.es/~julianp/2021-crm-course/section-10.html) to more general one (for general Kneser graph `KG(n,r,k)`).

## src/killed.jl & src/not_finished.jl & src/repsn-test.jl
Demonstrate the problem of getting a unitary representations of symmetric groups. The file `killed.jl` tries to use `GAP.Globals.UnitaryRepresentation` and tries to compute only the representations corresponding to partitions of the form `(n-a,a)` and only `n=6`. But it gets killed by the system with OOM. The file `not_finished.jl` tries to use `GAP.Globals.IrreducibleRepresentationsDixon`, but doesn't finish even for `n=6`.
