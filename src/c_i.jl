using Oscar, Combinatorics, Permutations

n = 5
r = 3
k = 1

p_r = Vector{Int}(undef,r)
for i in range(1,r)
  p_r[i]=i
end
p_p = Vector{Int}(undef, n-r)
for i in range(r+1,n)
  p_p[i-r] = i
end

cs = Vector{Permutation}()

for i in range(0,k)
  for c1 in combinations(p_r, i)
      for c2 in combinations(p_p, r-i)
        perm = Permutation(n)
        neighbor = vcat(c1,c2)
        for i in range(1,r)
          if i != neighbor[i]
            perm *= Transposition(n,i,neighbor[i])
          end
          #println(i, " ", neighbor[i], " ",perm)
        end
        push!(cs, perm)
      end
  end
end
  
for p in cs
  println(p)
end
  
