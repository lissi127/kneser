using GAP
GAP.Packages.load("RepnDecomp");
GAP.Packages.load("repsn")

n = 6

G = GAP.Globals.SymmetricGroup(n)

ct = GAP.Globals.CharacterTable(G);
irrct = GAP.Globals.Irr(ct)
params = GAP.Globals.CharacterParameters(ct);

irrchars = Vector{GapObj}()
for i in range(1, length(params))
  if length(params[i][2]) == 2
    push!(irrchars, irrct[i])
  end
end

irrs = [GAP.Globals.IrreducibleAffordingRepresentation(chi) for chi in irrchars]
GAP.Globals.SetCyclotomicsLimit(2^32-1)
uirrs = [ GAP.Globals.UnitaryRepresentation(irr) for irr in irrs ]

