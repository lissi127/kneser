LoadPackage("RepnDecomp");
LoadPackage("repsn");

G:=Group([(1,6)(2,5)(3,4)(7,8)(10,11), (1,4,9,10)(2,6,7,12)(3,5,8,11)]);
H:=Stabilizer(G,1);
irrs := IrreducibleRepresentations(G);
uirrs := List(irrs, r->UnitaryRepresentation(r));
# The order is not guaranteed, might change
coset_reps := [3,8,11];
Xset := Flat(List(coset_reps, i->List(RightCosets(G,H)[i])))
conns := List(coset_reps, i->Representative(RightCosets(G,H)[i]));
[ (1,10,9,4)(2,12,7,6)(3,11,8,5), (1,12,6,9)(2,11,4,8)(3,10,5,7), (1,3,2)(4,9,11)(5,7,12)(6,8,10) ]

