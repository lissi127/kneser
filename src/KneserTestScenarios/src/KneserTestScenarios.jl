module KneserTestScenarios

struct Scenario
    parsed_ereps::Dict{Vector{Int64}, Any}
    ugen_array::Vector{Vector{Matrix{ComplexF64}}}
    degrees::Vector{Int64}
    trivial_character_index::Int64
    H_perms::Vector{Vector{Int64}}
    connecting_gens::Vector{Vector{Int64}}
end

export
    Scenario,
    solve_sdp,
    K53_single_uirr_scenario_julia,
    Kneser_all_uirrs_scenario_julia,
    Kneser_restricted_uirrs_scenario_julia,
    truncated_tetrahedron_scenario_julia

println("loading packages in KneserTestScenarios...")
@time begin
    using Test, GAP, LinearAlgebra, Combinatorics, Permutations, VTSolverInterface

    push!(LOAD_PATH, pwd())
    using UnitaryIrrep, KneserGeneral    
end

# function output_gens(n, gens, ugens, parsed_ereps, irr)
#     jp1 = fill_gap_permutation(gens[1],n)
#     jp2 = fill_gap_permutation(gens[2],n)
#     println("parsed_ereps[gens[1] = $jp1]: ", parsed_ereps[jp1])
#     println("gap rep:")
#     display(gens[1]^irr)
#     println("\njulia rep:")
#     display(round.(ugens[1], digits=10))
#     println("\n\nparsed_ereps[gens[2] = $jp2]: ", parsed_ereps[jp2])
#     println("gap rep:")
#     display(gens[2]^irr)
#     println("\njulia rep:")
#     display(round.(ugens[2], digits=10))
# end    

function K53_single_uirr_scenario_julia(j::Int64)
    n = 5
    r = 3
    
    G = GAP.Globals.SymmetricGroup(n)
    GAP.Globals.SetCyclotomicsLimit(2^32-1)
    gens = GAP.Globals.GeneratorsOfGroup(G)

    ct = GAP.Globals.CharacterTable(G);
    irrchars = GAP.Globals.Irr(ct)

    println("Gap calculating IrreducibleRepresentations")
    @time irrs = GAP.Globals.IrreducibleRepresentations(G)
    irr = irrs[j]
    # println("irrchar: ", irrchars[j])
    # println("irr: ", irr) 
    
    H = GAP.evalstr("List(Group([(1,2),(1,2,3),(4,5)]))")
    
    parsed_ereps = parsed_element_representatives(G, n)
    ugens = unitary_generators(gens, parsed_ereps, irr)

#    output_gens(n, gens, ugens, parsed_ereps, irr)
    
    use_unitary_irrep(ugens)
    return [ n, G, gens, irr, parsed_ereps, ugens, H ]
end


# function K53_single_uirr_scenario_gap(j::Int64)
#     n = 5
#     r = 3
    
#     G = GAP.Globals.SymmetricGroup(n)
#     GAP.Globals.SetCyclotomicsLimit(2^32-1)
#     gens = GAP.Globals.GeneratorsOfGroup(G)

#     ct = GAP.Globals.CharacterTable(G);
#     irrchars = GAP.Globals.Irr(ct)
#     #    irrchars = GAP.Globals.Irr(G)

#     println("Gap calculating IrreducibleRepresentations")
#     @time irrs = GAP.Globals.IrreducibleRepresentations(G)
#     irr = irrs[j]
    
#     #irrchar = irrchars[j]
#     #println("Gap calculating IrreducibleAffordingRepresentation...")
#     #@time irr = GAP.Globals.IrreducibleAffordingRepresentation(irrs[j])
    
#     println("Gap calculating UnitaryRepresentation...")
#     @time uirr = GAP.Globals.UnitaryRepresentation(irr)
#     H = GAP.evalstr("List(Group([(1,2),(1,2,3),(4,5)]))")
    
#     parsed_ereps = parsed_element_representatives(G, n)
#     println("K53_single_uirr_scenario[$j]:")
#     jp1 = fill_gap_permutation(gens[1],n)
#     jp2 = fill_gap_permutation(gens[2],n)
#     println("parsed_ereps[gens[1] = $jp1]: ", parsed_ereps[jp1])
#     println("gap rep:")
#     display(gens[1]^irr)
#     println("\nparsed_ereps[gens[2] = $jp2]: ", parsed_ereps[jp2])
#     println("gap rep:")
#     display(gens[2]^irr)
#     tugens = transposed_unitary_generators(gens, parsed_ereps, irr)

#     use_unitary_irrep(tugens)
#     println("tugens for j=$j")
#     println(tugens)
#     return [ n, G, gens, irr, uirr, parsed_ereps, tugens, H ]
# end


# function K53_all_uirrs_scenario_gap()
#     n = 5
#     r = 3
#     k = 1
    
#     println("initializing group...")
#     @time begin
#         G = GAP.Globals.SymmetricGroup(n)
#         gens = GAP.Globals.GeneratorsOfGroup(G)
#         ct = GAP.Globals.CharacterTable(G)
#         irrchars = GAP.Globals.Irr(ct)
#         degrees = map(GAP.Globals.Degree, irrchars)
#     end

#     GAP.Globals.SetCyclotomicsLimit(2^32-1)

#     println("IrreducibleRepresentations..")
#     @time all_irrs_gap = GAP.Globals.IrreducibleRepresentations(G)

#     println("UnitaryRepresentations...")
#     irrs_gap = []
#     uirrs_gap = []
#     for i in [1,2,5,6,7]
#         println("  ", all_irrs_gap[i])
#         push!(irrs_gap, all_irrs_gap[i])
#         @time push!(uirrs_gap, GAP.Globals.UnitaryRepresentation(all_irrs_gap[i]))
#     end
    
        
#     H_list = GAP.evalstr("List(Group([(1,2),(1,2,3),(4,5)]))")
#     H_perms = [ fill_gap_permutation(h, n) for h in H_list ]
#     connecting_gens = create_connecting_gens(n, r, k)

#     println("preparing julia...")
#     @time parsed_ereps = parsed_element_representatives(G, n)

#     println("finding unitary irreps in julia...")
#     @time ugen_array = [ transposed_unitary_generators(gens, parsed_ereps, irr) for irr in irrs_gap ]

#     return [ n, gens, uirrs_gap, parsed_ereps, ugen_array, degrees, H_perms, connecting_gens ]
# end
    

# function truncated_tetrahedron_scenario_gap()
#     G = GAP.evalstr("Group([(1,6)(2,5)(3,4)(7,8)(10,11), (1,4,9,10)(2,6,7,12)(3,5,8,11)])")
#     H = GAP.Globals.Stabilizer(G,1)

#     ct = GAP.Globals.CharacterTable(G);
#     irrct = GAP.Globals.Irr(ct)
#     degrees = map(GAP.Globals.Degree, irrct)

#     irrs = GAP.Globals.IrreducibleRepresentations(G);
#     uirrs = [GAP.Globals.UnitaryRepresentation(irr) for irr in irrs]

#     n = 12
#     connecting_gens = [ fill_gap_permutation(c, n) for c in
#                            [ GAP.evalstr("(1,6)(2,4)(3,5)(7,10)(8,11)(9,12)"),
#                              GAP.evalstr("(1,2,3)(4,11,9)(5,12,7)(6,10,8)"),
#                              GAP.evalstr("(1,3,2)(4,9,11)(5,7,12)(6,8,10)") ]]

#     return [ n, uirrs, degrees, H, connecting_gens ]    
# end

function solve_sdp(s::Scenario)
    size_of_G = length(s.parsed_ereps)[1]
    repsums = [ UnitaryIrrep.sum_over_H(tugs, s.parsed_ereps, s.H_perms) for tugs in s.ugen_array ]
    nzrs = nonzero_rep_indices(repsums)

    @time value, vars = do_optimization(size_of_G, s.degrees, s.parsed_ereps, s.ugen_array, s.trivial_character_index, s.connecting_gens, repsums, nzrs)
    return value, vars
end

function truncated_tetrahedron_scenario_julia()
    G::GapObj = GAP.evalstr("Group([(1,6)(2,5)(3,4)(7,8)(10,11), (1,4,9,10)(2,6,7,12)(3,5,8,11)])")
    H_list::GapObj = GAP.evalstr("List(Stabilizer(Group([(1,6)(2,5)(3,4)(7,8)(10,11), (1,4,9,10)(2,6,7,12)(3,5,8,11)]), 1))")
    n::Int = 12
    H_perms::Vector{Vector{Int64}} = [ fill_gap_permutation(h, n) for h in H_list ]    

    id_func = x->true
    
    gd::GroupData = retrieve_or_calculate(G, n, "truncated-tet", id_func)


    # println("setup: gens of irrep 4:")
    # display(gens[1]^irrs_gap[4])
    # println()
    # display(gens[2]^irrs_gap[4])
    # println()    
    
    connecting_gens::Vector{Vector{Int64}} =
        [ fill_gap_permutation(c, n) for c in
             [ GAP.evalstr("(1,6)(2,4)(3,5)(7,10)(8,11)(9,12)"),
               GAP.evalstr("(1,2,3)(4,11,9)(5,12,7)(6,10,8)"),
               GAP.evalstr("(1,3,2)(4,9,11)(5,7,12)(6,8,10)") ]]

    return Scenario(gd.parsed_ereps, gd.ugen_array, gd.degrees, gd.trivial_character_index, H_perms, connecting_gens)
end

function Kneser_all_uirrs_scenario_julia(n::Int64, r::Int64, k::Int64)
    println("initializing group...")
    @time begin
        G::GapObj = GAP.Globals.SymmetricGroup(n)
        #        H_list = GAP.evalstr("List(Group([(1,2),(1,2,3),(4,5)]))")
        gap_stabilizer_string::String = "List(Stabilizer(SymmetricGroup($n), " * string([i for i in 1:r]) * ", OnSets))"
        println("gap_stabilizer_string: ", gap_stabilizer_string)
        H_list::GapObj = GAP.evalstr(gap_stabilizer_string)
        H_perms::Vector{Vector{Int64}} = [ fill_gap_permutation(h, n) for h in H_list ]
        connecting_gens::Vector{Vector{Int64}} = create_connecting_gens(n, r, k)
    end

    GAP.Globals.SetCyclotomicsLimit(2^32-1)

    id_func = x->true
    
    instance_name::String = "symmetric_$n"
    gd::GroupData = retrieve_or_calculate(G, n, instance_name, id_func)
    
    return Scenario(gd.parsed_ereps, gd.ugen_array, gd.degrees, gd.trivial_character_index, H_perms, connecting_gens)
    
end

function Kneser_restricted_uirrs_scenario_julia(n::Int64, r::Int64, k::Int64)
    println("initializing group...")
    @time begin
        G::GapObj = GAP.Globals.SymmetricGroup(n)
        gap_stabilizer_string::String = "List(Stabilizer(SymmetricGroup($n), " * string([i for i in 1:r]) * ", OnSets))"
        H_list::GapObj = GAP.evalstr(gap_stabilizer_string)
        H_perms::Vector{Vector{Int64}} = [ fill_gap_permutation(h, n) for h in H_list ]
        connecting_gens::Vector{Vector{Int64}} = create_connecting_gens(n, r, k)
    end

    GAP.Globals.SetCyclotomicsLimit(2^32-1)

    character_selector = function(param::GapObj)
        if length(param[2]) <= 2
            return true
        else
            return false
        end
    end
    
    instance_name::String = "symmetric_$n" * "_restricted"
    gd::GroupData = retrieve_or_calculate(G, n, instance_name, character_selector)
    
    return Scenario(gd.parsed_ereps, gd.ugen_array, gd.degrees, gd.trivial_character_index, H_perms, connecting_gens)
    
end

end
