module UnitaryIrrep

using GAP, LinearAlgebra, Permutations, SparseArrays, LDLFactorizations, Serialization
export
    fill_gap_permutation,
    parsed_element_representatives,
    unitary_generators,
    use_unitary_irrep,
    image_of,
    complex_gap_matrix_to_julia,
    sum_over_H,
    sum_AstarA,
    pivoted_cholesky,
    retrieve_or_calculate,
    GroupData

struct GroupData
    parsed_ereps::Dict{Vector{Int64}, Any}
    ugen_array::Vector{Vector{Matrix{ComplexF64}}}
    degrees::Vector{Int64}
    trivial_character_index::Int64
end

#=
The following code must go into the end of
~/.julia/packages/GAP/xxxxx/pkg/futil/lib/Decimal.gi
where xxxxx is the most recent directory:

DeclareGlobalFunction("RationalImaginaryPartOrZero");
InstallGlobalFunction( RationalImaginaryPartOrZero, function(a)
    if IsComplexNumber(a) then
        return Mantissa(ImaginaryPart(a)) / 10^Precision(ImaginaryPart(a));
    else
        return 0;
    fi;
end);

DeclareGlobalFunction("RealPartMatrix");
InstallGlobalFunction( RealPartMatrix, function(m)
    local n;
    n := Length(m);
    return List([1..n], i->List([1..n], j->Mantissa(RealPart(m[i,j])) / 10^Precision(RealPart(m[i,j]))));
end);

DeclareGlobalFunction("ImaginaryPartMatrix");
InstallGlobalFunction( ImaginaryPartMatrix, function(m)
    local n;
    n := Length(m);
    return List([1..n], i->List([1..n], j->RationalImaginaryPartOrZero(m[i,j])));
end);

DeclareGlobalFunction("ElementRepresentatives");
InstallGlobalFunction( ElementRepresentatives, function(G)
    local hom;
    hom := EpimorphismFromFreeGroup(G);
    return List(G, g->[String(ListPerm(g)), String(PreImagesRepresentative(hom,g))]);
end);

=#

# dummy variables for parsing
global x1::LinearAlgebra.Transpose{ComplexF64, Matrix{ComplexF64}}  
global x2::LinearAlgebra.Transpose{ComplexF64, Matrix{ComplexF64}}

#=
GAP permutations are represented in cycle form, and therefore
can be shorter than n if they leave the last several elements invariant.
We don't like that because our permutations are represented in explicit form
=#
function fill_gap_permutation(gtj_string::String, n::Int64)
    maybe_truncated_perm::Vector{Int64} = eval(Meta.parse(gtj_string))
    for k = size(maybe_truncated_perm)[1] : n-1
        push!(maybe_truncated_perm, k+1)
    end
    return maybe_truncated_perm
end

function fill_gap_permutation(g::GapObj, n::Int64)
    gstr = GAP.gap_to_julia(GAP.Globals.String(GAP.Globals.ListPerm(g)))
    if gstr == "[ ]"
        return [i for i in 1:n]
    else
        return fill_gap_permutation(gstr, n)
    end
end


#=
return a dictionary that maps each permutation, 
represented as an array of length n,
to a string expressing that permutation in terms of the
generators of the permutation group.

The actual work is done in the GAP function ElementRepresentatives
described above, this function just converts that data into
a form usable by julia
=#
function parsed_element_representatives(GG::GapObj, n::Int64)
    repstrs = Dict{Vector{Int64},Any}()
    for r in GAP.Globals.ElementRepresentatives(GG)
        gtj_string = GAP.gap_to_julia(r[1])

        # The identity is a special case
        if gtj_string == "[ ]"
            repstrs[Vector{Int64}(1:n)] = Meta.parse("I")
            continue
        end

        # now assign to the dict
        repstrs[fill_gap_permutation(gtj_string, n)] =
            Meta.parse(GAP.gap_to_julia(r[2]))
    end
    return repstrs
end

        
function sum_AstarA(parsed_ereps::Dict{Vector{Int64},Any})
    sum = zeros(ComplexF64, size(x1))
    for (perm, rep) in parsed_ereps
        # the transpose here is necessary because
        # - the rep string is expressed in matrices acting on rows
        # - the matrices x1, x2 are matrices acting on rows
        # - but in the end, we want a matrix acting on columns
        repmat = transpose(eval(rep)) # rep contains x1 and x2
        sum += repmat' * repmat
    end
    return sum
end

function complex_gap_matrix_to_julia(gen::GapObj)
    xa = GAP.Globals.DecimalApproximation(gen, 20) # 20 is the precision; this should be enough for 64 bit Floats
    xr = GAP.Globals.RealPartMatrix(xa)
    xr_vv = [[ComplexF64(x) for x in r] for r in GAP.gap_to_julia(xr)]

    xi = GAP.Globals.ImaginaryPartMatrix(xa)
    xi_vv = [[ComplexF64(x)*1.0im for x in r] for r in GAP.gap_to_julia(xi)]

    # in order to actually get the matrix coming from gap,
    # we would need to transpose because of how reduce(hcat, ...) works)
    # however, because gap matrices act on row vectors and julia matrices
    # on column vectors, we won't use a transpose here, and therefore
    # return to julia the transposed matrix of gap
    return reduce(hcat, xr_vv) + reduce(hcat, xi_vv)
end

function pivoted_cholesky(S::Matrix{ComplexF64})
    # This uses LDLFactorizations.jl:
    LDLT = ldl(S)
    L = LDLT.L;
    D = LDLT.D
    p = LDLT.P
    n = size(S)[1]

    # The invariant is that
    # P S P^top =   (L + I) D (L + I)*
    # where P is the permutation matrix corresponding to the vector p
    # therefore, since P^\top = P^*,
    # S =   P^{-1} (L+I)sqrt(D)    sqrt(D) (L+I)* P^{-*}
    #   = ( P^{-1} (L+I)sqrt(D) ) ( P^{-1}(L+I)sqrt(D) )*
    #   =           C                      C*

    pinv = inv(Permutation(p)).data

    # to efficiently calculate the inverse, convert it back to dense
    C = Matrix{ComplexF64}( permute( (L+I)*sqrt.(D), pinv, [i for i in 1:n] ) )

    # println("L:")
    # display(L)
    # println("\nD:")
    # display(D)
    # println("\np: ", p)    
    # println("C:")
    # display(C)
    # println("\nS - CC*:")
    # display(S - C*C')
    # println()

    return C
end

function unitary_generators(gens::GapObj,
                            parsed_ereps::Dict{Vector{Int64},Any},
                            irrep::GapObj)
    # x1, x2 will be matrices acting on rows
    # because they will be substituted into gap actions,
    # they need to be transposed back into gap form
    global x1 = transpose(complex_gap_matrix_to_julia(gens[1]^irrep))
    global x2 = transpose(complex_gap_matrix_to_julia(gens[2]^irrep))

    # bail early if the generators are already unitary
    if norm(x1' * x1 - I) < 1e-7 && norm(x2' * x2 - I) < 1e-7
        return [ transpose(x1), transpose(x2) ]
    end
    
    # println("\n\nunitary_generators: x1 in julia format= ", transpose(real.(x1)))
    # display(transpose(x1))
    # println("\nx2 in julia format= ", transpose(x2))
    # display(transpose(x2))
    # println()
    
    s = sum_AstarA(parsed_ereps) # uses x1, x2
    # println("sum s:");
    # display(s)
    # println()

    C = pivoted_cholesky(s)
    
    Cti = (C')^-1
    
    return [ C' * transpose(x1) * Cti,
             C' * transpose(x2) * Cti ]
end

function use_unitary_irrep(ugens::Vector{Matrix{ComplexF64}})
    if length(ugens) != 2
        println("can only handle the case of two generators")
        exit(1)
    end

    global x1 = transpose(ugens[1])
    global x2 = transpose(ugens[2])
    # println("\n\nuse_unitary_irrep: Used x1=")
    # display(x1)
    # println("\nx2=")
    # display(x2)
    # println()
end

function image_of(g::Vector{Int64}, parsed_ereps::Dict{Vector{Int64},Any})
#    println("image_of(g = $g), parsed_ereps: ", parsed_ereps[g])
    return transpose(eval(parsed_ereps[g]))
end

# irr, H
function sum_over_H(tugs::Vector{Matrix{ComplexF64}},
                    parsed_ereps::Dict{Vector{Int64}, Any},
                    H_perms::Vector{Vector{Int64}})
    use_unitary_irrep(tugs)
    return sum(image_of(h, parsed_ereps) for h in H_perms)
end

function calculate(G::GapObj, n::Int64, character_selector::Function)
    println("calculating characters:")
    irrchars::GapObj = GAP.Globals.Irr(G)
    n_chars::Int64 = length(irrchars)
    ct::GapObj = GAP.Globals.CharacterTable(G)

    # it could be that the group has no character parameters,
    # so the following call could fail
    # we preemptively substitute it with nonsense entries of length > 2    
    params = [ [ -1 for i=1:n_chars ] for i=1:n_chars ]
    
    try
        params = GAP.Globals.CharacterParameters(ct)
    catch
        println("caught")
    end

    #println("params: $params")

    degrees = Vector{Int64}()
        
    println("parsed element reps:")
    @time begin
        gens::GapObj = GAP.Globals.GeneratorsOfGroup(G)
        parsed_ereps::Dict{Vector{Int64}, Any} = parsed_element_representatives(G, n)
    end
    
    println("IrreducibleRepresentations..")
    @time irrs_gap::GapObj = GAP.Globals.IrreducibleRepresentations(G)

    println("Unitary reps in julia...")
    @time begin
        ones = [1 for i=1:n_chars]
        found_char_index::Int64 = 1
        ugen_array = Vector{Vector{Matrix{ComplexF64}}}()
        tci::Int64 = -1
        for i in range(1, length(params))
            if character_selector(params[i])
                ugens = unitary_generators(gens, parsed_ereps, irrs_gap[i])
                push!(ugen_array, ugens)
                push!(degrees, size(ugens[1])[1])
                if GAP.gap_to_julia(GAP.Globals.ValuesOfClassFunction(irrchars[i])) == ones
                    tci = found_char_index
                end
                found_char_index = found_char_index + 1
            end
        end
    end

    if tci == -1
        println("No trivial character accepted; this shouldn't happen")
        exit(1)
    end

    println("trivial character index found: ", tci)
    
    return GroupData(parsed_ereps, ugen_array, degrees, tci)
end

function retrieve_or_calculate(G::GapObj, n::Int64,
                               instance_name::String,
                               character_selector::Function)    
    if isfile(instance_name * ".group_data")
        group_data = deserialize(instance_name * ".group_data")
    else
        group_data = calculate(G, n, character_selector)
        serialize(instance_name * ".group_data", group_data)
    end
    
    return group_data
end

end # module
