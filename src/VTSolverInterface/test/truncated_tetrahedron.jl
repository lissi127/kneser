module GeneratorTests

println("loading packages...")
@time begin
    using Test, GAP, LinearAlgebra, Combinatorics, Permutations, Mosek, MosekTools, MathOptInterface, JuMP
    GAP.Packages.load("FUtil")
    GAP.Packages.load("RepnDecomp")
    GAP.Packages.load("repsn")

    push!(LOAD_PATH, pwd())
    using UnitaryIrrep, KneserGeneral, KneserTestScenarios, VTSolverInterface
end

println("set up group in julia")
@time begin
    n, gens, irrs_gap, parsed_ereps, ugen_array, degrees, H_perms, connecting_gens = truncated_tetrahedron_scenario_julia()
end


println("setup...")
@time begin
    global model = Model(optimizer_with_attributes(Mosek.Optimizer))

    n_irreps = length(irrs_gap)
    for i in range(1,n_irreps)
        d = degrees[i]
        eval(Meta.parse("@variable(model, X$i[1:$d,1:$d], PSD)"))
    end
    X = [ eval(Meta.parse("X$i")) for i=1:n_irreps ]
end

@testset "size constraint" begin
    println("make constraint...")
    @time sc = size_constraint(degrees, X, n_irreps)

    ref_constr = eval(Meta.parse("X1[1,1] + X2[1,1] + 2* X3[1,1] + 2* X3[2,2] + 3* X4[1,1] + 3* X4[2,2] + 3* X4[3,3] + 3* X5[1,1] + 3* X5[2,2] + 3* X5[3,3]"))

    ref_constr -= sc
    
    @test ref_constr.constant == 0
    @test norm([ v for (k,v) in ref_constr.terms]) ≈ 0 atol=1e-7
end
# transposed version
# ref_constrs =
#     [ [eval(Meta.parse(c[1])),
#        eval(Meta.parse(c[2]))] for c in
#            [ [ "2 * X1[1,1] + 2 * X3[1,1] + 4 * X3[1,2] + 2 * X3[2,2] - 3 * X4[1,1] + 6 * X4[1,2] - 3 * X4[2,2] + 6 * X4[3,3] - 3 * X5[1,1] - 6 * X5[1,2] - 3 * X5[2,2]",
#                "0"],
#              [ "2 * X1[1,1] - X3[1,1] - 2 * X3[1,2] - X3[2,2] - 3 * X4[1,2] + 3 * X4[2,2] - 9 * X4[1,3] + 3 * X4[2,3] - 3 * X5[1,2] - 3 * X5[2,2] + 3 * X5[1,3] + 3 * X5[2,3]",
#                "-1.7320508075688772 * X3[1,1] + 1.7320508075688772 * X3[2,2]" ],
#              [ "2 * X1[1,1] - X3[1,1] - 2 * X3[1,2] - X3[2,2] - 3 * X4[1,3] + 9 * X4[2,3] + 3 * X4[1,1] - 3 * X4[1,2] - 3 * X5[1,3] - 3 * X5[2,3] - 3 * X5[1,1] - 3 * X5[1,2]",
#                "1.7320508075688772 * X3[1,1] - 1.7320508075688772 * X3[2,2]"] ] ]

ref_constrs =
    [ [eval(Meta.parse(c[1])),
       eval(Meta.parse(c[2]))] for c in
           [ [ "2 * X1[1,1] + 2 * X3[1,1] + 4 * X3[1,2] + 2 * X3[2,2] - 3 * X4[1,1] + 6 * X4[1,2] - 3 * X4[2,2] + 6 * X4[3,3] - 3 * X5[1,1] - 6 * X5[1,2] - 3 * X5[2,2]",
               "0"],
             [ "2 * X1[1,1] - X3[1,1] - 2 * X3[1,2] - X3[2,2] - 3 * X4[1,3] + 9 * X4[2,3] + 3 * X4[1,1] - 3 * X4[1,2] - 3 * X5[1,3] - 3 * X5[2,3] - 3 * X5[1,1] - 3 * X5[1,2]",
               "-1.7320508075688772 * X3[1,1] + 1.7320508075688772 * X3[2,2]" ],
             [ "2 * X1[1,1] - X3[1,1] - 2 * X3[1,2] - X3[2,2] - 3 * X4[1,2] + 3 * X4[2,2] - 9 * X4[1,3] + 3 * X4[2,3] - 3 * X5[1,2] - 3 * X5[2,2] + 3 * X5[1,3] + 3 * X5[2,3]",
               "1.7320508075688772 * X3[1,1] - 1.7320508075688772 * X3[2,2]"] ] ]

# @testset "connecting constraints in gap" begin
#     println("preparing...")
#     @time begin
#         global gap_repsums = [ KneserGeneral.sum_over_H(uirr.unitary_rep, H) for uirr in uirrs ]
#         global gap_nzrs = nonzero_rep_indices(gap_repsums)
#     end

#     println("making connecting gen constraints in gap...")
#     @time cg_constraints = connecting_gen_constraints_gap(connecting_gens_gap, gap_nzrs, degrees, uirrs, gap_repsums, X)
#     # for constr in cg_constraints
#     #     println(constr)
#     # end
    
#     for i=1:3
#         for j=1:2
#             diff = cg_constraints[i][j] - ref_constrs[i][j]
#             @test diff.constant == 0
#             @test norm([ v for (k,v) in diff.terms]) ≈ 0 atol=1e-7
#         end
#     end
    
# end


@testset "compare repsums" begin
    println("preparing...")
    @time begin
        global julia_repsums = [ UnitaryIrrep.sum_over_H(tugs, parsed_ereps, H_perms) for tugs in ugen_array ]
        global julia_nzrs = nonzero_rep_indices(julia_repsums)
    end

    # global gap_repsums, gap_nzrs
    
    # @test gap_nzrs == julia_nzrs
    # for i=1:length(gap_nzrs)[1]
    #     @test norm(gap_repsums[i] - julia_repsums[i]) ≈ 0 atol=1e-7 
    # end
end

# @testset "compare definitions of connecting_gens" begin
#     @test length(connecting_gens_gap) == length(connecting_gens_julia)
#     for i=1:length(connecting_gens_julia)[1]
#         @test connecting_gens_gap[i] == connecting_gens_julia[i]
#     end
# end

# @testset "compare images of connecting_gens" begin
#     global julia_nzrs
#     for j=1:length(connecting_gens_julia)[1]
#         for i in julia_nzrs 
#             im_gap = KneserGeneral.image_of(connecting_gens_gap[j], uirrs[i].unitary_rep)
#             use_unitary_irrep(ugen_array[i])
#             im_julia = UnitaryIrrep.image_of(connecting_gens_julia[j], parsed_ereps)
#             # println(connecting_gens_julia[j])
#             # display(im_gap)
#             # println()
#             # display(im_julia)
#             # println()
#             @test norm(transpose(im_gap) - im_julia) ≈ 0 atol=1e-7
#         end
#     end
# end

@testset "connecting constraints in julia" begin
    println("making connecting gen constraints...")
    @time cg_constraints = connecting_gen_constraints(connecting_gens, julia_nzrs, degrees, parsed_ereps, ugen_array, julia_repsums, X)

    for i=1:3
        for j=1:2
            diff = cg_constraints[i][j] - ref_constrs[i][j]
            @test diff.constant == 0
            @test norm([ v for (k,v) in diff.terms]) ≈ 0 atol=1e-7 
        end
    end
    
end

# @testset "optimization" begin
#     size_of_G = length(parsed_ereps)[1]
#     println("size_of_G:  $size_of_G")

#     global julia_repsums, julia_nzrs
#     @time value = do_optimization(size_of_G, degrees, parsed_ereps, ugen_array, connecting_gens_julia, julia_repsums, julia_nzrs)
#     @test value ≈ 5 atol=1e-7
# end

end
