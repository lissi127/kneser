module GeneratorTests

println("loading packages...")
@time begin
    using Test, GAP, LinearAlgebra, Combinatorics, Permutations, Mosek, MosekTools, MathOptInterface, JuMP
    GAP.Packages.load("FUtil")
    GAP.Packages.load("RepnDecomp")
    GAP.Packages.load("repsn")

    push!(LOAD_PATH, pwd())
    using UnitaryIrrep, KneserGeneral, KneserTestScenarios, VTSolverInterface
end

println("set up group in julia")
@time begin
    n, parsed_ereps, ugen_array, degrees, H_perms, connecting_gens = K53_all_uirrs_scenario_julia()
end

println("set up repsums")
@time begin
    println("old connecting_gens: ", connecting_gens)
    connecting_gens = [ (Permutation(c)^-1).data for c in [ [1,4,5,2,3], [2,4,5,1,3], [3,4,5,1,2] ] ]
    println("replaced connecting_gens: ", connecting_gens)
    
    size_of_G = length(parsed_ereps)[1]
    repsums = [ UnitaryIrrep.sum_over_H(tugs, parsed_ereps, H_perms) for tugs in ugen_array ]
    nzrs = nonzero_rep_indices(repsums)
end

println("set up solver...")
@time begin
    solver = optimizer_with_attributes(Mosek.Optimizer)
    model = Model(solver)
    n_irreps = length(ugen_array)
    for i = 1:n_irreps
        d = degrees[i]
        eval(Meta.parse("@variable(model, X$i[1:$d,1:$d], PSD)"))
    end
    X = [ eval(Meta.parse("X$i")) for i=1:n_irreps ]
end

i = 5
rs = round.(repsums[i], digits=10)
println("repsum: ", rs)
display(rs)
println()
use_unitary_irrep(ugen_array[i])

println("\nx1 in gap format: ", transpose(ugen_array[i][1]))
println("\nx2 in gap format: ", transpose(ugen_array[i][2]))

for cg in connecting_gens
    println("\nconnecting_gen $cg")
end

# perms_giving = Dict{JuMP.GenericAffExpr{ComplexF64, JuMP.VariableRef},
#                     Vector{Vector{Int64}}}()
# for (p, parsed) in parsed_ereps
#     #imgen = round.(UnitaryIrrep.image_of(p, parsed_ereps), digits=10)
#     # println("\nimage of connecting_gen: ", imgen)
#     # display(imgen)
#     # println()
#     cp = separate_reim(scaled_tip(degrees[i],
#                                   Astar_times_B(repsums[i], X[i]),
#                                   UnitaryIrrep.image_of(p, parsed_ereps)))

#     if haskey(perms_giving, cp[1])
#         old_list = perms_giving[cp[1]]
#         push!(old_list, p)
#         perms_giving[cp[1]] = old_list
#     else
#         perms_giving[cp[1]] = [ p ]
#     end
# #    println("$p\nre cp: ", cp[1])
# #    println("im cp: ", cp[2], "\n\n")
# end

# for (k,v) in perms_giving
#     println("perm $k")
#     for x in v
#         println("  ", Permutation(x), " with inverse ", Permutation(x)^-1)
#     end
# end


end
