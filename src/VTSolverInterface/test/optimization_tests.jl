module OptimizationTests

function print_solution(vars)
    println("vars:")
    for i=1:length(vars)
        println("X$i:\n")
        display(round.(vars[i], digits = 8))
        println("\n")
    end
end    

println("loading packages...")

@time begin
    using Test, GAP, LinearAlgebra, Combinatorics, Permutations, Mosek, MosekTools, MathOptInterface, JuMP
    GAP.Packages.load("FUtil")
    GAP.Packages.load("RepnDecomp")
    GAP.Packages.load("repsn")

    push!(LOAD_PATH, pwd())
    using UnitaryIrrep, KneserGeneral, KneserTestScenarios, VTSolverInterface
end

@testset "truncated tetrahedron" begin
#     value = solve_sdp(truncated_tetrahedron_scenario_julia)
    @time scenario = truncated_tetrahedron_scenario_julia()
    value, vars = solve_sdp(scenario)
    print_solution(vars)
    @test value ≈ 8 atol=1e-7
end

@testset "K53 in julia" begin
    println("constructing scenario...")
    @time scenario = Kneser_all_uirrs_scenario_julia(5,3,1)
    value, vars = solve_sdp(scenario)
    print_solution(vars)
    @test value ≈ 48 atol=1e-7
end

@testset "K53, restricted scenario in julia" begin
    println("constructing scenario...")
    @time scenario = Kneser_restricted_uirrs_scenario_julia(5,3,1)
    value, vars = solve_sdp(scenario)
    print_solution(vars)
    @test value ≈ 48 atol=1e-7
end

end
