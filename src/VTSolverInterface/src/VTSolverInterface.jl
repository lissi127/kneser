module VTSolverInterface

using KneserGeneral, Mosek, MosekTools, MathOptInterface, JuMP, GAP, LinearAlgebra

global model::Model

const MOI = MathOptInterface

export do_optimization

function do_optimization(size_of_g::Int64,
                         degrees::Vector{Int64},
                         parsed_ereps::Dict{Vector{Int64}, Any},
                         ugen_array::Vector{Vector{Matrix{ComplexF64}}},
                         trivial_character_index::Int64,
                         connecting_gens::Vector{Vector{Int64}},
                         repsums::Vector{Matrix{ComplexF64}},
                         nzrs::Vector{Int64})
    #println("Setting up optimizer...")
    #@time
    begin
        solver::MathOptInterface.OptimizerWithAttributes = optimizer_with_attributes(Mosek.Optimizer)
        #    global model::Model = Model(solver)
        global model = Model(solver)
        
        # construct variables for the optimalization
        #-------------------------

        n_irreps = length(ugen_array)
        for i = 1:n_irreps
            d = degrees[i]
            eval(Meta.parse("@variable(model, X$i[1:$d,1:$d], PSD)"))
        end

        
        #=
        @variable(model, X1[1:1,1:1])
        @variable(model, X2[1:4,1:4], PSD)
        @variable(model, X3[1:5,1:5], PSD)
        @variable(model, X4[1:6,1:6], PSD)
        @variable(model, X5[1:5,1:5], PSD)
        @variable(model, X6[1:4,1:4], PSD)
        @variable(model, X7[1:1,1:1])
        =#

        #X=[X1,X2,X3,X4,X5,X6,X7]
        # X = Vector{Symmetric{VariableRef, Matrix{VariableRef}}}()
        # for i in range(1,n_irreps)
        #     push!(X, eval(Meta.parse("X$i")))
        # end

        #X::Vector{Symmetric{VariableRef, Matrix{VariableRef}}} = [ eval(Meta.parse("X$i")) for i=1:n_irreps ]
        X = [ eval(Meta.parse("X$i")) for i=1:n_irreps ]

        #-------------------------
    end

    # println("construct first constraint...")
    # @time sc::AffExpr = size_constraint(degrees, X, n_irreps)
    # @time
    sc = size_constraint(degrees, X, n_irreps)

    #println("adding it to the model...")
    #@time begin
    @constraint(model, sc == size_of_g)
    #end
    
    # println("build connecting constraints...")
    # @time
    cg_constraints = connecting_gen_constraints(connecting_gens, nzrs, degrees, parsed_ereps, ugen_array, repsums, X)

    # println("add connecting constraints to model...")
    # @time
    begin
        for c in cg_constraints
            @constraint(model, c[1] == 0)
            if c[2] != 0
                @constraint(model, c[2] == 0)
            end
        end
    end


    #@objective(model, Max, X7[1,1])
    @objective(model, Max, eval(Meta.parse("X"*string(trivial_character_index)*"[1,1]")))
    # println("model:\n")
    # println(model)
    optimize!(model)

    value = MOI.get(model, MOI.ObjectiveValue())
    vars = [JuMP.value.(eval(Meta.parse("X$i"))) for i=1:n_irreps]
    
    println("Result of optimizer: ", value)
    return value, vars
end

end # module
