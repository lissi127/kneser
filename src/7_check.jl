using Oscar, RepresentationTheory, Permutations, Plots, LinearAlgebra

n = 7
r = 3

s_n = symmetric_group(n)
s_r = symmetric_group(r)
s_p = symmetric_group(n-r)

# Create group S_r x S_{n-r}
#-------------------------

trans = cperm(s_n,[1])

for i in range(1,n-r)
    perm = cperm(s_n,[i,r+i])
    global trans = perm*trans
end

#println(trans)

v = Vector{PermGroupElem}()
for p in s_p
  push!(v,trans^-1*p*trans)
end

H = Vector{Permutation}()
for p in s_r
  for p2 in v
    push!(H,Permutation(Vector(s_n(p)*p2)))
  end
end

#Create all partitions of n
#-------------------------

partitions = Vector{Vector{Int64}}()
for p in AbstractAlgebra.Generic.partitions(n)
  push!(partitions, Vector(p))
end

#println(partitions)

#-------------------------
is_not_zero = Vector{Vector{Int64}}()

for p in partitions
  println(p)
  
  # compute dimension of representation
  repre = Representation(RepresentationTheory.Partition(p))
  dim = size(repre(Permutation(Vector(trans))))[1]
  println(dim)
  
  #sums all representations of H
  sum = Matrix(1.0I, dim, dim)
  for i in range(2,length(H))
    sum += Matrix(repre(H[i]))
  end

  #checks if the sum is nearly zero
  is_zero = true
  for i in range(1,size(sum)[1])
    for j in range(1,size(sum)[2])
      is_zero &= isapprox(sum[i,j], 0.0; atol=1e-10, rtol=0)
      #is_zero &= isapprox(sum[i,j], 0.0; atol=eps(Float64), rtol=0)
    end
  end
  println(is_zero)
  if ~is_zero
    push!(is_not_zero, p)
  end
  
  println()
end

println("The sum is non-zero for these partitions:")
for p in is_not_zero
  println(p)
end
