using GAP
GAP.Packages.load("RepnDecomp");

n=6
G = GAP.Globals.SymmetricGroup(n)
irrs = GAP.Globals.IrreducibleRepresentations(G)

half = div(n,2)+1
len = length(irrs)

GAP.Globals.SetCyclotomicsLimit(2^32-1)
uirrs = [ GAP.Globals.UnitaryRepresentation(irrs[i]) for i in range(len-half+1, len) ]
#uirrs = [ GAP.Globals.UnitaryRepresentation(irr) for irr in irrs ]
