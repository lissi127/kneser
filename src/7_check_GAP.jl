using GAP, JuMP
GAP.Packages.load("RepnDecomp");
GAP.Packages.load("FUtil");

n = 5
r = 3
k = 0

# Create group S_r x S_{n-r}
#-------------------------
s = "Group(["
if r > 1
  s *= "(1,2)"
  if r > 2  
    s *= ","
    s *= string(([r:-1:1;]...,))
  end
end

s *= ","

if n-r > 1
  s *= string((r+1,r+2))
  if n-r > 2
    s *= ","
    s *= string(([n:-1:r+1;]...,))
  end
end

s *= "])"
H = GAP.evalstr(s)
println(H)
#-------------------------
function matrix_to_julia(gap_mat)
  gap_approx_mat_string =
    GAP.Globals.String(GAP.Globals.DecimalApproximation(gap_mat))
  hcat((Meta.parse(String(gap_approx_mat_string))|>eval)...)'
end

G = GAP.Globals.SymmetricGroup(n)
irrs = GAP.Globals.IrreducibleRepresentations(G)
uirrs = [ GAP.Globals.UnitaryRepresentation(irr) for irr in irrs ]

irrchars = GAP.Globals.Irr(G)
n_irreps = GAP.Globals.Size(irrs)

for i =1:n_irreps
  dim = GAP.Globals.Degree(irrchars[i])
  println(dim)
  suma = zeros(dim, dim)
  for h in H
    repre = matrix_to_julia(h^uirrs[i].unitary_rep)
    suma += repre
  end
  #println(suma)
  
  #checks if the sum is nearly zero
  is_zero = true
  for i in range(1, dim)
    for j in range(1, dim)
      is_zero &= isapprox(suma[i,j], 0.0; atol=1e-8, rtol=0)
      #is_zero &= isapprox(sum[i,j], 0.0; atol=eps(Float64), rtol=0)
    end
  end
  println(is_zero)
  println("char ", irrchars[i])
end
