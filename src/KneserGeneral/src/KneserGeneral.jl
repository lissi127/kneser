module KneserGeneral

using GAP, JuMP, LinearAlgebra, Combinatorics, Permutations

push!(LOAD_PATH, pwd())
using UnitaryIrrep

export
    create_product_group,
    create_connecting_gens,
    image_of,
    sum_over_H,
    make_repsums,
    trace_inner_product,
    nonzero_rep_indices,
    size_constraint,
    connecting_gen_constraints,
    Astar_times_B,
    scaled_tip,
    separate_reim

n_rounding_digits::Int = 10
imag_part_tol::Float64 = 1e-10

# Create group S_r x S_{n-r}
#-------------------------
function create_product_group(G::GapObj, n::Int64, r::Int64)
    s = "Group(["
    if r > 1
        s *= "(1,2)"
        if r > 2  
            s *= ","
            s *= string(([r:-1:1;]...,))
        end
    end

    s *= ","

    if n-r > 1
        s *= string((r+1,r+2))
        if n-r > 2
            s *= ","
            s *= string(([n:-1:r+1;]...,))
        end
    end

    s *= "])"
    #H = GAP.evalstr("Group([(1,2),(1,2,3),(4,5)])")
    return GAP.evalstr(s)
end
#-------------------------

# create cs - permutations 
#-------------------------
function create_connecting_gens(n::Int64, r::Int64, k::Int64)
    p_r = Vector{Int}(undef,r)
    for i in range(1,r)
        p_r[i]=i
    end
    p_p = Vector{Int}(undef, n-r)
    for i in range(r+1,n)
        p_p[i-r] = i
    end

    cs = Vector{Permutation}()

    for i in range(0,k)
        for c1 in combinations(p_r, i)
            for c2 in combinations(p_p, r-i)
                perm = Permutation(n)
                neighbor = vcat(c1,c2)
                for i in range(1,r)
                    if i != neighbor[i]
                        perm *= Transposition(n,i,neighbor[i])
                    end
                end
                # need inverse because of column, not row operation
                # so the map says how to get to the identity,
                # not where the identity goes
                push!(cs, perm^-1) 
            end
        end
    end
    #    return [GAP.Globals.PermList(GAP.evalstr(string(c.data))) for c in cs]
    return [c.data for c in cs] # These are Vector{Int64}
end

function image_of(h::GapObj, irrep::GapObj)
    return complex_gap_matrix_to_julia(h^irrep)
end

function image_of(g::Vector{Int64}, irrep::GapObj)
    return complex_gap_matrix_to_julia(GAP.Globals.PermList(GAP.evalstr(string(g)))^irrep)
end

function nonzero_rep_indices(repsums::Vector{Matrix{ComplexF64}}, tol::Float64=1.0e-10)
    nzrs = Vector{Int64}()
    for i=1:length(repsums)
        if norm(repsums[i]) > tol
            push!(nzrs, i)
        end
    end
    return nzrs
end

function sum_over_H(irr::GapObj, H::GapObj)
    return sum(image_of(h, irr) for h in H)
end

function make_repsums_gap(uirrs::Vector{GapObj}, H::GapObj)
    return [ sum_over_H(irr.unitary_rep, H) for irr in uirrs ]
end

# this calculates the scaled trace inner product
# lambda * tr(A^\star B) = \lambda * sum_{i,j} a_{ij}^\star b_{ij}
function scaled_tip(lambda::Int64,
                    A::Matrix{JuMP.GenericAffExpr{ComplexF64, JuMP.VariableRef}},
                    B::LinearAlgebra.Transpose{ComplexF64, Matrix{ComplexF64}})
    n::Int64 = size(A)[1]
    sum::JuMP.GenericAffExpr{ComplexF64, JuMP.VariableRef} = 0
    for i = 1:n
        for j = 1:n
            sum += lambda * conj(A[i,j]) * B[i,j]
        end
    end
    return sum
end

function scaled_tip(lambda::Int64,
                    A::Matrix{JuMP.GenericAffExpr{ComplexF64, JuMP.VariableRef}},
                    B::Matrix{ComplexF64})
    n::Int64 = size(A)[1]
    sum::JuMP.GenericAffExpr{ComplexF64, JuMP.VariableRef} = 0
    for i = 1:n
        for j = 1:n
            sum += lambda * conj(A[i,j]) * B[i,j]
        end
    end
    return sum
end

function scaled_tip(lambda::Int64,
                    A::Matrix{JuMP.GenericAffExpr{ComplexF64, JuMP.VariableRef}},
                    B::LinearAlgebra.UniformScaling{Bool})
    n::Int64 = size(A)[1]
    sum::JuMP.GenericAffExpr{ComplexF64, JuMP.VariableRef} = 0
    for i = 1:n
        for j = 1:n
            sum += lambda * conj(A[i,j])
        end
    end
    return sum
end

# this cannot be expressed as simply A' * B
# because of 
function Astar_times_B(A::Matrix{ComplexF64},
                       B::Symmetric{VariableRef, Matrix{VariableRef}})
    if size(A) != size(B)
        println("size mismatch for A=")
        display(A)
        println("\nB=")
        display(B)
        println()
        exit(1)
    end

    n::Int64 = size(A)[1]
    AstarB = zeros(JuMP.GenericAffExpr{ComplexF64, JuMP.VariableRef}, n, n)
    for i = 1:n
        for j = 1:n
            for k = 1:n
                AstarB[i,j] += conj(A[k,i]) * B[k,j] # A is conjugate-transposed
            end
        end
    end
    return AstarB
end


function separate_reim(constraint::JuMP.GenericAffExpr{ComplexF64, JuMP.VariableRef})
    global n_rounding_digits
    global imag_part_tol
    
    re_constraint::JuMP.AffExpr = real(constraint.constant)
    im_constraint::JuMP.AffExpr = 0
    if abs(imag(constraint.constant)) > imag_part_tol
        im_constraint = round(imag(constraint.constant), digits = n_rounding_digits)
    end
    
    for (var, coeff) in constraint.terms
        re_constraint += round(real(coeff), digits = n_rounding_digits) * var
        if abs(imag(coeff)) > imag_part_tol
            im_constraint += imag(coeff) * var
        end
    end

    return [re_constraint, im_constraint]
end

    
function size_constraint(degrees::Vector{Int64},
                         X::Vector{Symmetric{VariableRef, Matrix{VariableRef}}},
                         n_irreps::Int64)
    return sum(degrees[i] * tr(X[i]) for i=1:n_irreps)
end

function connecting_gen_constraints(connecting_gens::Vector{Vector{Int64}},
                                    nzrs::Vector{Int64},
                                    degrees::Vector{Int64},
                                    parsed_ereps::Dict{Vector{Int64}, Any},
                                    ugen_array::Vector{Vector{Matrix{ComplexF64}}},
                                    repsums::Vector{Matrix{ComplexF64}},
                                    X::Vector{Symmetric{VariableRef, Matrix{VariableRef}}})
    cg_constraints::Vector{Vector{AffExpr}} = []
    for cg in connecting_gens
        # println("\n\nprocessing connecting gen ", cg)
        constraint_pairs = Vector{Vector{AffExpr}}()
        for i in nzrs
            use_unitary_irrep(ugen_array[i])

            # println("inside with irrep $i")
            # println("var_coeff_product_transpose:")
            # display(var_coeff_product_transpose(X[i], repsums[i]))
            # println("\nimage of gen ", cg)
            # display(round.(UnitaryIrrep.image_of(cg, parsed_ereps), digits=10))
            # println()

            cp = separate_reim(scaled_tip(degrees[i],
                                          Astar_times_B(repsums[i], X[i]),
                                          UnitaryIrrep.image_of(cg, parsed_ereps)))
            # println("re cp: ", cp[1])
            # println("im cp: ", cp[2])
            push!(constraint_pairs, cp)
        end
        # println("re sum: ", sum(c[1] for c in constraint_pairs))
        # println("im sum: ", sum(c[2] for c in constraint_pairs))
        push!(cg_constraints, [ sum(c[1] for c in constraint_pairs),
                                sum(c[2] for c in constraint_pairs) ])
    end
    return cg_constraints
end

end # module
