module GeneratorTests

println("This package is deactivated until the precise relation between gap and julia formulations is established.")
exit(1)

println("loading packages...")
@time begin
    using Test, GAP, LinearAlgebra, Combinatorics, Permutations
    GAP.Packages.load("FUtil")
    GAP.Packages.load("RepnDecomp")
    GAP.Packages.load("repsn")

    GAP.Globals.SetCyclotomicsLimit(2^32-1)
    
    push!(LOAD_PATH, pwd())
    using UnitaryIrrep, KneserGeneral, KneserTestScenarios
end

function sum_over_H_with_gap(H, uirr)
    return 
end    

function run_single_uirr_scenario(n, G, gens, irr, uirr, parsed_ereps, H)
    g5_gap = KneserGeneral.image_of(gens[1], uirr.unitary_rep)
    g2_gap = KneserGeneral.image_of(gens[2], uirr.unitary_rep)

    p1 = fill_gap_permutation(gens[1], n)
    g5_julia = UnitaryIrrep.image_of(p1, parsed_ereps)
    p2 = fill_gap_permutation(gens[2], n)
    g2_julia = UnitaryIrrep.image_of(p2, parsed_ereps)

    @testset "GAP periods" begin
        @test norm(g5_gap^5-I) ≈ 0 atol=1e-10
        @test norm(g2_gap^2-I) ≈ 0 atol=1e-10
    end

    @testset "julia periods" begin
        @test norm(g5_julia^5-I) ≈ 0 atol=1e-10
        @test norm(g2_julia^2-I) ≈ 0 atol=1e-10
    end

    @testset "conversion_gap" begin
        conv_mat = [ complex_gap_matrix_to_julia(gens[i]^uirr.unitary_rep) for i=1:2 ]
        @test norm(g5_gap - conv_mat[1]) ≈ 0 atol=1e-10
        @test norm(g2_gap - conv_mat[2]) ≈ 0 atol=1e-10
    end

    @testset "sum_AstarA" begin
        use_unitary_irrep([transpose(complex_gap_matrix_to_julia(gens[1]^irr)),
                           transpose(complex_gap_matrix_to_julia(gens[2]^irr))])
        s_julia = sum_AstarA(parsed_ereps)
        s1_gap = sum(transpose(complex_gap_matrix_to_julia(g^irr)) * (transpose(complex_gap_matrix_to_julia(g^irr)))' for g in G)
        s2_gap = sum((transpose(complex_gap_matrix_to_julia(g^irr)))' * transpose(complex_gap_matrix_to_julia(g^irr)) for g in G)
        println("\ns_julia:")
        display(s_julia)
        println("\ns1_gap:")
        display(s1_gap)
        println("\ns2_gap:")
        display(s2_gap)
        @test norm(s_julia - s1_gap) ≈ 0 atol=1e-10
        @test norm(s_julia - s2_gap) ≈ 0 atol=1e-10
    end
    
    @testset "conversion_julia" begin
        conv_mat = [ complex_gap_matrix_to_julia(gens[i]^uirr.unitary_rep) for i=1:2 ]
        @test norm(g5_julia - conv_mat[1]) ≈ 0 atol=1e-10
        @test norm(g2_julia - conv_mat[2]) ≈ 0 atol=1e-10
    end
    
    @testset "generators" begin
        for g in [gens[1], gens[2]]
            g_gap = transpose(KneserGeneral.image_of(g, uirr.unitary_rep))
            p = fill_gap_permutation(g,n)
            g_julia = UnitaryIrrep.image_of(p, parsed_ereps)

            println("g = $g")
            println("g_gap:")
            display(round.(g_gap, digits = 10))
            println()
            println("g_julia:")
            display(round.(g_julia, digits = 10))
            println()
            
            @test norm(g_gap - g_julia) ≈ 0 atol=1e-10
        end
    end

    @testset "subgroup" begin
        for g in H
            g_gap = transpose(KneserGeneral.image_of(g, uirr.unitary_rep))
            g_julia = UnitaryIrrep.image_of(fill_gap_permutation(g,n),
                                            parsed_ereps)
            p = fill_gap_permutation(g,n)
            @test norm(g_gap - g_julia) ≈ 0 atol=1e-10
        end
    end

    @testset "sum over H" begin
        sG = sum(transpose(KneserGeneral.image_of(h, uirr.unitary_rep)) for h in H)
        sJ = sum(UnitaryIrrep.image_of(fill_gap_permutation(h,n),
                                       parsed_ereps) for h in H)
        @test norm(sG - sJ) ≈ 0 atol=1e-10
    end

end

#for j in [5,6,7]
begin
    j=6
    println("preparing K53_single_uirr_scenario $j...")
    @time n, G, gens, irr, uirr, parsed_ereps, tugens, H = K53_single_uirr_scenario(j)
#    gap_gens = [ KneserGeneral.image_of(g, uirr.unitary_rep) for g in gens ]
#    use_unitary_irrep(gap_gens) ## OJO
    
    run_single_uirr_scenario(n, G, gens, irr, uirr, parsed_ereps, H)
end

    
@testset "repsums" begin
    println("Preparing K53_all_uirrs_scenario...")
    n, gens, uirrs_gap, parsed_ereps, ugen_array, degrees, H_perms, connecting_gens = K53_all_uirrs_scenario()

    #### OJO
#    ugen_array = [ [ KneserGeneral.image_of(g, uirr.unitary_rep) for g in gens ] for uirr in uirrs_gap ]

    println("gap sums")
    @time ref_repsums = [ KneserGeneral.sum_over_H(irr.unitary_rep, H) for irr in uirrs_gap ]
    # for r in ref_repsums
    #     display(r)
    #     println("\n")
    # end

    println("julia sums")
    @time soH = [ UnitaryIrrep.sum_over_H(tugs, parsed_ereps, H_perms) for tugs in ugen_array ]
    # for r in soH
    #     display(r)
    #     println("\n")
    # end

    for i=1:size(soH)[1]
        @test norm(transpose(ref_repsums[i]) - soH[i]) ≈ 0 atol=1e-10
    end
end

end
