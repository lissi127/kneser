module JuliaTests

println("loading packages...")
@time begin
    using Test, GAP, LinearAlgebra, Combinatorics, Permutations, SparseArrays, LDLFactorizations
    GAP.Packages.load("FUtil")
    GAP.Packages.load("RepnDecomp")

    
    push!(LOAD_PATH, pwd())
    using UnitaryIrrep, KneserGeneral, KneserTestScenarios
end

function run_single_uirr_scenario(n, gens, periods, irr, parsed_ereps, H)
    p1 = fill_gap_permutation(gens[1], n)
    g5 = UnitaryIrrep.image_of(p1, parsed_ereps)
    p2 = fill_gap_permutation(gens[2], n)
    g2 = UnitaryIrrep.image_of(p2, parsed_ereps)

    @testset "is transposed" begin
    #     for i=1:2 
    #         println("\ngens[$i]^irr: ")
    #         display(gens[i]^irr)
    #         println("\ncomplex_gap_matrix_to_julia:")
    #         display(complex_gap_matrix_to_julia(gens[i]^irr))
    #         println()
    #     end
    #     # don't know how to actually test this,
    #     # because don't know how to directly convert a gap matrix with E()'s
    #     # into julia without going through the own code
    #     # so this will be a visual inspection
    end
    
    @testset "periods" begin
        @test norm(g5^periods[1]-I) ≈ 0 atol=1e-10
        @test norm(g2^periods[2]-I) ≈ 0 atol=1e-10
        if size(g5)[1] > 1
            @test norm(g5^(periods[1]-1)-I) > 1e-10
            @test norm(g2^(periods[2]-1)-I) > 1e-10
        end
    end

    @testset "generator unitarity" begin
        @test norm(g5'-g5^-1) ≈ 0 atol=1e-10
        @test norm(g2'-g2^-1) ≈ 0 atol=1e-10
    end

end

# begin
#     println("preparing truncated_tetrahedron_scenario_julia...")
#     @time n, gens, irrs_gap, parsed_ereps, ugen_array, degrees, H_perms, connecting_gens = truncated_tetrahedron_scenario_julia()

#     for i=1:size(ugen_array)[1]
#         use_unitary_irrep(ugen_array[i])
#         run_single_uirr_scenario(n, gens, [2,4], irrs_gap[i], parsed_ereps, H_perms)
#     end
    
# end

@testset "cholesky" begin
    for s in [
        [240.0 120.0 120.0 0; 120 240 0 0; 120 0 240 120; 0 0 120 240],
        [240.0 80 -80 80 -80; 80 240 -80 -80 80; -80 -80 240 80 80; 80 -80 80 240 -80; -80 80 80 -80 240],
        [240.0 -80 -80 80 80 0; -80 240 0 80 -80 -80; -80 0 240 -80 80 0; 80 80 -80 240 0 0; 80 -80 80 0 240 -80; 0 -80 0 0 -80 240]
        ]
        C = pivoted_cholesky(Matrix{ComplexF64}(s))
        @test norm(s - C*C') ≈ 0 atol=1e-10
    end
end


for j=1:7
    println("preparing K53_single_uirr_scenario_julia $j...")
    @time n, G, gens, irr, parsed_ereps, ugens, H = K53_single_uirr_scenario_julia(j)
    
    run_single_uirr_scenario(n, gens, [5,2], irr, parsed_ereps, H)
end

end
