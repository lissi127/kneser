if size(ARGS)[1] == 0 
    println("usage: ", PROGRAM_FILE, " <degree n of symmetric group>")
    exit(0);
end

n = parse(Int64, ARGS[1])
println("Using n=", n)

println("Loading GAP...")
@time using GAP
println("Loading GAP packages...")
@time begin
    GAP.Packages.load("RepnDecomp")
    GAP.Packages.load("repsn")
    GAP.Packages.load("FUtil")
end
println("Loading other packages...")
@time using LinearAlgebra

println("Loading own package from ", pwd(), "...")
push!(LOAD_PATH, pwd())
@time using UnitaryIrrep

println("Preparing group data...")
@time begin
    GG = GAP.Globals.SymmetricGroup(n)
    parsed_ereps = parsed_element_representatives(GG, n)
    ct = GAP.Globals.CharacterTable(GG)
    irrct = GAP.Globals.Irr(ct)
    params = GAP.Globals.CharacterParameters(ct)
end

for i in range(1, length(params))
    if length(params[i][2]) == 2
        println("\nparameters for i=", i, ": ", params[i][2])
        @time ugens = unitary_generators(GG, parsed_ereps, irrct[i])
        println("unitary x1:\n", ugens[1])
        println("its trace: ", tr(ugens[1]))
        println("unitary x2:\n", ugens[2])
        println("its trace: ", tr(ugens[2]))
    end
end
