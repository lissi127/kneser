# println("Loading GAP...")
# @time using GAP
# println("Loading other packages...")
# @time using LinearAlgebra

println("Loading own packages from ", pwd(), "...")
push!(LOAD_PATH, pwd())
@time using VTSolverInterface, KneserGeneral, UnitaryIrrep

println("Loading GAP packages...")
@time begin
    import GAP
    GAP.Packages.load("RepnDecomp")
    GAP.Packages.load("repsn")
    GAP.Packages.load("FUtil")
end

n = 5
r = 3
k = 1

println("Calculating group data...")
@time begin
    G = GAP.Globals.SymmetricGroup(n)

    #connecting_gens = [ GAP.evalstr("(2,4)(3,5)"), GAP.evalstr("(1,2,4)(3,5)"), GAP.evalstr("(1,3,5,2,4)") ]
    #-------------------------
    connecting_gens = create_connecting_gens(n, r, k)

    H = create_product_group(G, n, r)
    println("H in gap:")
    println(H)
    println("ElementRepresentatives:")
# here    println(parsed_element_representatives(H, 
    
    C = GAP.Globals.RightCosets(G,H)
    n_cosets = GAP.Globals.Size(C)

    ct = GAP.Globals.CharacterTable(G);
    irrct = GAP.Globals.Irr(ct)
    params = GAP.Globals.CharacterParameters(ct);

    irrchars = Vector{GAP.GapObj}()
    for i in range(1, length(params))
        #if length(params[i][2]) == 2
        push!(irrchars, irrct[i])
        #end
    end

    #degrees::Vector{Int64} = map(GAP.Globals.Degree, irrchars)
    degrees = map(GAP.Globals.Degree, irrchars)

    #irrs::GAP.GapObj = GAP.Globals.IrreducibleRepresentations(G)
    irrs = GAP.Globals.IrreducibleRepresentations(G)
end

println("Comupting uirrs")
@time begin
    #    GAP.Globals.SetCyclotomicsLimit(2^32-1)
    #    uirrs::Vector{GAP.GapObj} = [ GAP.Globals.UnitaryRepresentation(irr) for irr in irrs ]
    #     n_irreps::Int64 = length(irrs)
    uirrs = [ GAP.Globals.UnitaryRepresentation(irr) for irr in irrs ]
    gens = GAP.Globals.GeneratorsOfGroup(G)
    parsed_ereps = parsed_element_representatives(G, n)
    ugen_array = [transposed_unitary_generators(gens, parsed_ereps, irr) for irr in irrs] 
end

println("Call optimizer...")
@time value = do_optimization(G, H, degrees, uirrs, connecting_gens)

println("Lower bound for chromatic number: ", value*n_cosets/factorial(n))


